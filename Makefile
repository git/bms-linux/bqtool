LOCAL_SRC_FILES:= main.c expression-parser.c gauge.c interface-linux-i2c.c gauge-simulator.c
LOCAL_CFLAGS += -Wno-error=sign-compare
#LOCAL_LDFLAGS := --gc-sections
LOCAL_MODULE := bqtool
LOCAL_STATIC_LIBRARIES :=  -lc -lm

all:
	$(CC) $(LOCAL_SRC_FILES) $(LOCAL_CFLAGS) -o $(LOCAL_MODULE) $(LOCAL_STATIC_LIBRARIES)

clean:
	rm -f $(LOCAL_MODULE) 