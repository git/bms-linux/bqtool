ifneq ($(TARGET_SIMULATOR),true)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES:= main.c expression-parser.c gauge.c interface-linux-i2c.c gauge-simulator.c
LOCAL_CFLAGS += -Wno-error=sign-compare
#LOCAL_LDFLAGS := --gc-sections
LOCAL_MODULE := bqtool
LOCAL_STATIC_LIBRARIES := libcutils libc libm
include $(BUILD_EXECUTABLE)

endif  # TARGET_SIMULATOR != true
